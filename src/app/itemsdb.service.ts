import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ShoppingItem } from '../shoppingitem';

const dbName: string = 'items';

@Injectable({
  providedIn: 'root'
})
export class ItemsdbService {

  constructor(private storage: Storage) { }

  getAll() {
    return new Promise((resolve, reject) => {
      this.storage.get('items').then(data => {
        //return data from storage
        resolve(data);
      }, error => {
        // There is an error, call reject and pass error message.
        reject('Eroor retrieving data from storage');
      });
    });
  }

  save(items: Array<ShoppingItem>) {
    this.storage.set('items', JSON.stringify(items));
  }
}
